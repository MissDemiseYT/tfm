﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

    private bool optionMenu = false;
    public GameObject hideObject;
    private SoundManager sm;

    void Start()
    {
        if (hideObject != null)
        {
            hideObject.SetActive(false);
        }

        sm = GameObject.FindGameObjectWithTag("Sound").GetComponent<SoundManager>();
    }

    public void ChangeLevel(int level)
    {
        SceneManager.LoadScene("Level" + level);

        sm.ButtonSound();
    }

    public void Options()
    {
        SceneManager.LoadScene("Options");

        sm.ButtonSound();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1.0f;

        sm.ButtonSound();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    // OPEN THE ATTACHED OBJECT 
    public void OptionButtonMenu()
    {
        if (!optionMenu)
        {
            optionMenu = true;
        }
        
        if (optionMenu)
        {
            hideObject.SetActive(true);
            Time.timeScale = 0.0f;

            sm.ButtonSound();
        }
    }

    // HIDE THE OBJECT ATTACHED
    public void hideObjectButton()
    {
        hideObject.SetActive(false);
        Time.timeScale = 1.0f;

        sm.ButtonSound();
    }
}
