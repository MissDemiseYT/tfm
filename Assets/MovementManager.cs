﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour {

    // TODO: Añadir patrón para ir a una lista de puntos
    // TODO: Añadir patrón para que siga al jugador
    // TODO: Patrón que la nave gire entorno a si misma y luego vaya a un punto, gire sobre si misma y vaya a otro punto...
    // TODO: Patrón nave que viene, se planta y dispara quieta hacia donde el jugador esté (rotando solo)
    // TODO: Patrón nave que va moviéndose girando entorno a un punto (lo ideal es hacerlo con más naves, formando un círculo entyre ellas)
    // TODO: Patrón nave que está siempre a una distancia del jugador mirando hacia él y disparandom rotando al rededor suyo
    // TODO: Patrón nave que va a un punto y solo zigzaguea en un eje. (Ejemplo: Quieto en el eje y, pero haciendo: izquierda, derecha, izquierda, derecha en una amplitud)

    public Vector3 initialPosition;

    public float xSpeed = 0.01f;
    public float ySpeed = 1.0f;

    public float amplitude = 1.0f;

    public bool zigzag = false;

    private void Start()
    {
        initialPosition = gameObject.transform.position;
    }
    // Update is called once per frame
    void Update () {
        if (zigzag)
        {
            gameObject.transform.Translate(xSpeed, -ySpeed * Time.deltaTime, 0, Space.World);
            if (gameObject.transform.position.x > initialPosition.x + amplitude)
            {
                xSpeed = -xSpeed;
            }
            else if (gameObject.transform.position.x < initialPosition.x - amplitude)
            {
                xSpeed = -xSpeed;
            }
        } else
        {
            gameObject.transform.Translate(0, -ySpeed * Time.deltaTime, 0, Space.World);
        }
	}

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Finish"))
        {
            Destroy(gameObject);
        }
    }
}
