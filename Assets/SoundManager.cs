﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    private static bool created = false;

    // SOUNDS
        // AudioClips
    public AudioClip buttonClip;
    public AudioClip backgroundMusicClip;
        // AudioSources
    public AudioSource effectsSource;
    public AudioSource musicSource;


    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            Debug.Log("Awake: " + this.gameObject);
            musicSource.Play();
        }
    }

    public void ButtonSound()
    {
        effectsSource.PlayOneShot(buttonClip);
    }
}
